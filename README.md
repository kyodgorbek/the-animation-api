# the-animation-api
Animation API

Q.animations('player' , {
  run_right: { frames: _.range(0,10 ) },
  run_left:   { frames: _.range(10,20) },
  stand:      { frames: _.range (30,25), rate: 1/5},
  fire:         { frames: _.range(30,45),  loop: false, rate: 1/30 },
  die:         { frames: _.range(30,45),   rate: 1/5,  next: 'dead' },
  died:       { frames:   [ 45 ] }	
)};
